import React from 'react';
import { TouchableOpacity, TouchableOpacityProps, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';

interface CustomBackButtonProps extends TouchableOpacityProps { }

const CustomBackButton: React.FC<CustomBackButtonProps> = ({ }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon name="chevron-back-outline" size={30} />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
});

export default CustomBackButton;
