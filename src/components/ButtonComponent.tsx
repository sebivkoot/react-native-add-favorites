import { PressableProps, StyleSheet, Text, View, Pressable } from 'react-native'
import React from 'react'

interface ButtonComponentProps extends PressableProps {
    title: string,
    onPress: any
}

const ButtonComponent: React.FC<ButtonComponentProps> = ({
    title,
    onPress,
    ...rest
}) => {

    return (
        <Pressable
            {...rest}
            onPress={onPress}
            style={{
                borderWidth: 2,
                borderColor: rest.disabled ? 'grey' : '#287dde',
                borderRadius: 8,
                padding: 5,
                margin: 10,
                height: 40
            }}
        >
            <Text style={{
                fontSize: 20,
                textAlign: 'center'
            }}>{title}</Text>
        </Pressable>
    )
}

export default ButtonComponent

const styles = StyleSheet.create({})