import { StyleSheet, TextInput, TextInputProps, View, TextStyle, Text, Pressable } from 'react-native'
import React, { useState } from 'react'
import Icon from 'react-native-vector-icons/Ionicons';

interface InputComponentProps extends TextInputProps {
    label: string;
    inputStyle?: TextStyle;
    suffixIcon?: boolean;
    touched?: any;
    error?: any;
    refs?: any
}

const InputComponent: React.FC<InputComponentProps> = ({
    label,
    inputStyle,
    suffixIcon,
    touched,
    error,
    refs,
    ...rest
}) => {

    const [secure, setSecure] = useState(false || suffixIcon);

    return (
        <View>
            {label ?
                <Text style={styles.label}>
                    {label}
                </Text>
                : null}
            <TextInput
                {...rest}
                ref={refs}
                secureTextEntry={secure}
                style={[inputStyle, {
                    fontSize: 18,
                    borderColor: touched && error ? 'red' : 'gray',
                    borderWidth: 1,
                    borderRadius: 10,
                    padding: 10,
                    margin: 10,
                    paddingRight: suffixIcon ? 30 : 10
                }]}
            />
            {suffixIcon ?
                <Pressable
                    onPress={() => setSecure(!secure)}
                    style={styles.eyeIcon}>
                    {secure ? (
                        <Icon name="eye-outline" size={20} />
                    ) : (
                        <Icon name="eye-off-outline" size={20} />
                    )}
                </Pressable> : null}
            {touched && error ? (
                <Text style={styles.errorLabel}>
                    {error}
                </Text>
            ) : null}
        </View>
    )
}

export default InputComponent

const styles = StyleSheet.create({
    label: {
        fontSize: 18,
        marginLeft: 10, paddingBottom: 2,
        paddingTop: 5
    },
    eyeIcon: {
        position: 'absolute',
        right: 20,
        top: 50,
    },
    errorLabel: {
        fontSize: 14,
        color: 'red',
        paddingHorizontal: 10
    }
})