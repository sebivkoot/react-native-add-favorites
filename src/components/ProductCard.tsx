import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { NavigationProp } from '@react-navigation/native';
import { RootStackParamList } from '../navigation/AppNavigation';

interface ProductCardProps {
    item: {
        id: number;
        title: string;
        thumbnail: string;
    };
    navigation: NavigationProp<RootStackParamList>
}

const ProductCard: React.FC<ProductCardProps> = ({ item, navigation }) => {
    return (
        <TouchableOpacity
            key={item?.id}
            style={{
                flex: 1,
            }}
            onPress={() => navigation.navigate('ProductDetailsScreen', {
                title: item?.title,
                thumbnail: item?.thumbnail,
                id: item?.id
            })}>
            <View style={styles.card}>
                <Text style={{ fontSize: 20 }}>{item?.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

export default ProductCard

const styles = StyleSheet.create({
    card: {
        flex: 1,
        borderColor: '#0099ff',
        borderWidth: 2,
        margin: 10,
        borderRadius: 10,
        minHeight: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }
})