import { FlatList, StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { useFavoritesContext } from '../context/FavoritesProvider';
import FavouriteImageCard from '../components/FavouriteProductListCard';

interface FavouritesScreenProps { }

const FavouritesScreen: React.FC<FavouritesScreenProps> = ({ }) => {

    const { favorites } = useFavoritesContext();

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.title}>Favourites Screen</Text>
            </View>
            <FlatList
                data={favorites}
                keyExtractor={(item) => item?.id.toString()}
                renderItem={({ item }) => (
                    <FavouriteImageCard key={item.id} item={item} />
                )}
            />
        </View>
    )
}

export default FavouritesScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 2,
        margin: 2,
    },
    header: {
        borderBottomColor: 'black',
        borderBottomWidth: 2
    },
    title: {
        textAlign: 'center',
        fontSize: 20,
        padding: 10,
        fontWeight: 'bold',
        color: '#0099ff'
    },
    card: {
        flex: 1,
        borderColor: 'black',
        borderWidth: 2,
        margin: 10,
        borderRadius: 10,
        justifyContent: 'center',
    },
    closeButton: {
        borderWidth: 2,
        height: 60,
        backgroundColor: 'blue',
        borderRadius: 10,
        padding: 10,
        margin: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
})