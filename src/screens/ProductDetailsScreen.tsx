import { Alert, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../navigation/AppNavigation';
import { useFavoritesContext } from '../context/FavoritesProvider';

interface ProductDetailsScreenProps {
    navigation: NavigationProp<RootStackParamList, 'ProductDetailsScreen'>,
    route: RouteProp<RootStackParamList, 'ProductDetailsScreen'>
}

const ProductDetailsScreen: React.FC<ProductDetailsScreenProps> = ({ route }) => {

    const { addToFavorites, isFavorite } = useFavoritesContext();
    console.log(route.params, 'route.params')

    const handleAddToFavourite = () => {

        const newItem = {
            id: route.params.id,
            thumbnail: route.params.thumbnail,
            title: route.params.title
        };
        console.log(newItem, 'newItem:::::')

        if (!isFavorite(newItem.id)) {
            addToFavorites(newItem);
            Alert.alert(
                'Success',
                'Added to Favourites',
                [
                    {
                        text: 'OK',
                    },
                ],
            );
        }
        else {
            Alert.alert(
                'Validation Error',
                'Already in Favourites',
                [
                    {
                        text: 'OK',
                    },
                ],
            );
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text
                    style={styles.title}
                >Product Details</Text>
            </View>
            <Image
                style={{ height: 240 }}
                source={{ uri: route.params?.thumbnail }} />
            <Text style={styles.description}>{route.params?.title}</Text>
            <TouchableOpacity
                onPress={() => handleAddToFavourite()}
                style={styles.button}>
                <Text style={{
                    fontSize: 18,
                    color: 'white'
                }}>Add to favourites</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ProductDetailsScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        margin: 5,
    },
    header: {
        borderBottomColor: 'black',
        borderBottomWidth: 2,
        marginBottom: 10
    },
    title: {
        textAlign: 'center',
        fontSize: 20,
        padding: 10,
        fontWeight: 'bold',
        color: '#0099ff'
    },
    button: {
        borderWidth: 2,
        height: 60,
        backgroundColor: '#555',
        borderRadius: 10,
        padding: 10,
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    description: {
        fontSize: 18,
        textAlign: 'center',
        padding: 10
    }
})