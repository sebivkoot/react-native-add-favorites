import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { NavigationProp, RouteProp } from '@react-navigation/native';
import { RootStackParamList } from '../navigation/AppNavigation';
import PlaceCard from '../components/ProductCard';

interface HomeScreenProps {
    navigation: NavigationProp<RootStackParamList, 'HomeScreen'>,
    route: RouteProp<RootStackParamList, 'HomeScreen'>
}

const HomeScreen: React.FC<HomeScreenProps> = ({ navigation }) => {

    const [data, setData] = useState([])

    useEffect(() => {
        fetch('https://dummyjson.com/products')
            .then(response => response.json())
            .then(json => setData(json.products))
            .catch(error => {
                console.error('Error fetching data:', error);
            });
    }, [])

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.title}>List Of Products</Text>
            </View>
            <FlatList
                data={data}
                renderItem={({ item }: any) => (
                    <PlaceCard key={item.id} item={item} navigation={navigation} />
                )}
            />
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 2,
        margin: 2,
    },
    header: {
        borderBottomColor: 'black',
        borderBottomWidth: 2
    },
    title: {
        textAlign: 'center',
        fontSize: 20,
        padding: 10,
        fontWeight: 'bold',
        color: '#0099ff'
    }
})