import { Keyboard, KeyboardAvoidingView, Pressable, StyleSheet, Text, View } from 'react-native'
import React, { useRef } from 'react'
import InputComponent from '../components/InputComponent'
import { NavigationProp } from '@react-navigation/native'
import { RootStackParamList } from '../navigation/AppNavigation'
import { useFormik } from 'formik'
import * as  Yup from 'yup'
import ButtonComponent from '../components/ButtonComponent'

interface SignUpScreenProps {
    navigation: NavigationProp<RootStackParamList, 'SignUpScreen'>
}

const SignUpScreen: React.FC<SignUpScreenProps> = ({ navigation }) => {

    const emailRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);
    const confirmPasswordRef = useRef<HTMLInputElement>(null);

    const handleLoginInPress = () => {
        navigation.navigate('LoginScreen')
    };

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            confirmPassword: ''
        },
        validationSchema: Yup.object().shape({
            email: Yup.string()
                .email('Entered email is invalid')
                .required('Email is required'),
            password: Yup.string().required('Password is required')
                .min(8, "Must be 8 characters or more"),
            confirmPassword: Yup.string()
                .required('Confirm password is required')
                .oneOf([Yup.ref("password")], "Passwords does not match"),
        }),
        onSubmit: async values => {
            let data = {
                email: values.email,
                password: values.password,
                confirmPassword: values.confirmPassword
            }
            Keyboard.dismiss();
            console.log(data, 'data in SignIN Submit')
            try {
                navigation.navigate('LoginScreen')
                console.log('Submit successful');
            } catch (error) {
                console.error('Error in onSubmit:', error);
            }
        }
    })

    const {
        handleChange,
        handleSubmit,
        values,
        setFieldTouched,
        touched,
        errors,
        isValid,
        dirty,
    } = formik;

    const onSignUpSubmit = () => {
        setFieldTouched('confirmPassword')
        handleSubmit();
    }

    return (
        <View style={{ flex: 1 }}>
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                <Text style={styles.title}>Sign Up</Text>
                <View style={{ flex: 1, marginTop: 50 }}>
                    <InputComponent
                        label='Email'
                        refs={emailRef}
                        value={values.email}
                        placeholder="Enter email"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        onChangeText={handleChange('email')}
                        onBlur={() => setFieldTouched('email')}
                        error={errors.email}
                        touched={touched.email}
                        returnKeyType="next"
                        onSubmitEditing={() => {
                            passwordRef.current?.focus();
                        }}
                    />
                    <InputComponent
                        label='Password'
                        refs={passwordRef}
                        value={values.password}
                        placeholder="Enter password"
                        autoCapitalize="none"
                        onChangeText={handleChange('password')}
                        onBlur={() => setFieldTouched('password')}
                        secureTextEntry
                        touched={touched.password}
                        error={errors.password}
                        onSubmitEditing={() => {
                            confirmPasswordRef.current?.focus();
                        }}
                        suffixIcon
                    />
                    <InputComponent
                        label='Confirm Password'
                        refs={confirmPasswordRef}
                        value={values.confirmPassword}
                        placeholder="Enter password"
                        autoCapitalize="none"
                        onChangeText={handleChange('confirmPassword')}
                        onBlur={() => setFieldTouched('confirmPassword')}
                        secureTextEntry
                        touched={touched.confirmPassword}
                        error={errors.confirmPassword}
                        // onSubmitEditing={() => handleSubmit;
                        // }
                        suffixIcon
                    />
                    <ButtonComponent
                        title={'Sign Up'}
                        onPress={() => onSignUpSubmit()}
                    // disabled={!isValid || !dirty}
                    />
                    <View style={{ flexDirection: "row", justifyContent: 'center', padding: 10 }}>
                        <Text style={{ fontSize: 18, textAlign: 'center' }}>
                            Already have an account?
                        </Text>
                        <Pressable onPress={handleLoginInPress}>
                            <Text style={styles.hyperlink}>LogIn</Text>
                        </Pressable>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </View>
    )
}

export default SignUpScreen


const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        textAlign: 'center',
        margin: 10,
        paddingTop: 30,
        color: '#0099ff'
    },
    hyperlink: {
        paddingLeft: 5,
        fontSize: 18,
        textAlign: 'center',
        textDecorationLine: 'underline'
    }
})