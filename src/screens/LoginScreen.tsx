import { KeyboardAvoidingView, Pressable, StyleSheet, Text, View } from 'react-native'
import React, { useRef } from 'react'
import InputComponent from '../components/InputComponent'
import { NavigationProp } from '@react-navigation/native'
import { RootStackParamList } from '../navigation/AppNavigation'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import ButtonComponent from '../components/ButtonComponent'

interface LoginScreenProps {
    navigation: NavigationProp<RootStackParamList, 'LoginScreen'>
}

const LoginScreen: React.FC<LoginScreenProps> = ({ navigation }) => {

    const emailRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object().shape({
            email: Yup.string()
                .email('Entered email is invalid')
                .required('Email is required'),
            password: Yup.string()
                .required('Password is required')
        }),
        onSubmit: async values => {
            let data = {
                email: values.email,
                password: values.password,
            }
            console.log(data, 'data:::: Formik OnSubmit')
            try {
                navigation.navigate('TabNavigation')
                console.log('Submit successful');
            } catch (error) {
                console.error('Error in onSubmit:', error);
            }
        }
    })

    const {
        handleChange,
        handleSubmit,
        values,
        setFieldTouched,
        touched,
        errors,
        isValid,
        dirty,
    } = formik;

    const onLoginSubmit = () => {
        setFieldTouched('password', true);
        handleSubmit();
    }


    return (
        <View style={{ flex: 1 }}>
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                <Text style={styles.title}>Login</Text>
                <View style={{ flex: 1, marginTop: 30 }}>
                    <InputComponent
                        label='Email'
                        refs={emailRef}
                        value={values.email}
                        placeholder="Enter email"
                        autoCapitalize="none"
                        keyboardType="email-address"
                        onChangeText={handleChange('email')}
                        onBlur={() => setFieldTouched('email')}
                        touched={touched.email}
                        error={errors.email}
                        onSubmitEditing={() => {
                            passwordRef.current?.focus();
                        }}
                        returnKeyType="next"
                    />
                    <InputComponent
                        label='Password'
                        refs={passwordRef}
                        value={values.password}
                        placeholder="Enter password"
                        autoCapitalize="none"
                        onChangeText={handleChange('password')}
                        onBlur={() => setFieldTouched('password')}
                        touched={touched.password}
                        secureTextEntry
                        error={errors.password}
                        // onSubmitEditing={() => handleSubmit()
                        // }
                        suffixIcon
                    />
                    <ButtonComponent
                        title={'Login'}
                        onPress={onLoginSubmit}
                    // disabled={!isValid || !dirty}
                    />
                    <View style={{ flexDirection: "row", justifyContent: 'center', padding: 10 }}>
                        <Text style={{ fontSize: 18, textAlign: 'center' }}>
                            Don't have an account?
                        </Text>
                        <Pressable onPress={() => navigation.navigate('SignUpScreen')}>
                            <Text style={styles.hyperlink}>SignUp</Text>
                        </Pressable>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        textAlign: 'center',
        margin: 10,
        paddingTop: 30,
        color: '#0099ff'
    },
    hyperlink: {
        paddingLeft: 5,
        fontSize: 18,
        textAlign: 'center',
        textDecorationLine: 'underline'
    }
})