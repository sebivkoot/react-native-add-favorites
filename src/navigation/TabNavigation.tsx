import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import HomeScreen from '../screens/HomeScreen';
import { RootStackParamList } from './AppNavigation';
import FavouritesScreen from '../screens/FavouritesScreen';
import Icon from 'react-native-vector-icons/Ionicons';

interface TabNavigationProps { }

const Tab = createBottomTabNavigator<RootStackParamList>();

const TabNavigation: React.FC<TabNavigationProps> = ({ }) => {
    return (
        <Tab.Navigator
            screenOptions={{ headerShown: false }}
        >
            <Tab.Screen
                options={{
                    title: 'Cities',
                    tabBarLabel: 'Cities',
                    tabBarIcon: ({ focused, color, size }) => {
                        return <Icon name='home' color={color} size={size} />
                    },
                }}
                name="HomeScreen"
                component={HomeScreen} />
            <Tab.Screen
                options={{
                    title: 'Favourites',
                    tabBarLabel: 'Favourites',
                    tabBarIcon: ({ focused, color, size }) => {
                        return <Icon name='star' color={color} size={size} />
                    },
                }
                }
                name="FavouritesScreen"
                component={FavouritesScreen} />
        </Tab.Navigator >
    );
}

export default TabNavigation