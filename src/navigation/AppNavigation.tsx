import { StyleSheet } from 'react-native';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ProductDetailsScreen from '../screens/ProductDetailsScreen';
import TabNavigation from './TabNavigation';
import CustomBackButton from '../components/CustomBackButton';
import LoginScreen from '../screens/LoginScreen';
import SignUpScreen from '../screens/SignUpScreen';

interface AppNavigationProps { }

export type RootStackParamList = {
    HomeScreen: undefined,
    TabNavigation: undefined,
    ProductDetailsScreen: {
        title: string;
        thumbnail: string;
        id: number;
    },
    FavouritesScreen: undefined;
    LoginScreen: undefined;
    SignupScreen: undefined;
}

const Stack = createNativeStackNavigator<RootStackParamList>();

const AppNavigation: React.FC<AppNavigationProps> = ({ }) => {
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName='LoginScreen'
                screenOptions={{
                    headerLeft: () => <CustomBackButton />,
                    headerTitle: '',
                }}
            >
                <Stack.Screen
                    options={{ headerShown: false }}
                    name='LoginScreen' component={LoginScreen} />
                <Stack.Screen
                    options={{ headerShown: false }}
                    name='SignupScreen' component={SignUpScreen} />
                <Stack.Screen
                    name='TabNavigation' component={TabNavigation} />
                <Stack.Screen
                    name='ProductDetailsScreen' component={ProductDetailsScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default AppNavigation

const styles = StyleSheet.create({})