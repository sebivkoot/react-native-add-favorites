type State = { place: number }

type Action = { type: 'INCREMENT' } | { type: 'DECREMENT' }

export const counterReducer = (state: State, action: Action): State => {
    switch (action.type) {
        case 'DECREMENT':
            return { place: state.place - 1 }
            break;
        case 'INCREMENT':
            return { place: state.place + 1 }
            break;

        default:
            throw new Error("Unknown action performed");
    }
}